package it.unibo.oop.graphSearch;

public class Node<N>{
	private boolean visited;
	private N nodeType;
	
	public Node(N nodeName){
		this.visited = false;
		this.nodeType = nodeName;
	}
	
	public boolean getVisited() {
		return this.visited;
	}
	
	public boolean checkVisit() {
		if(this.getVisited()) {
			return true;
		}
		return false;
	}
	public boolean visitNode() {
		if(!this.checkVisit()) {
			this.visited = true;
		}
		return false;
	}

	public N getNodeType() {
		return nodeType;
	}

	
	
}