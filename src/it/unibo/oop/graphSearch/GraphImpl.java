package it.unibo.oop.graphSearch;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GraphImpl<N> implements Graph<N>{
	private Set<Node> nodeSet = new HashSet<>();
	private Set<Edge> edgeSet = new HashSet<>();
	private List<N> path = new LinkedList<>();

	public void addNode(N node) { 
		this.nodeSet.add(new Node<N>(node));
	}

	public void addEdge(N source, N target) {
		this.edgeSet.add(new Edge(new Node<N>(source), new Node<N>(target)));
	}

	public Set<N> nodeSet() {
		Set<N> nodes = new HashSet<>();
		for(Node n: nodeSet) {
			nodes.add((N) n.getNodeType());
		}
		
		return nodes;
	}

	public Set<N> linkedNodes(N node) {
		Set<N> linkedEdgeSet = new HashSet<>();
		
		for(Edge nodePair: edgeSet) {
			if(this.evaluateNodes(nodePair, node) != null) {
				linkedEdgeSet.add(evaluateNodes(nodePair, node));
			}
		}
		
		return linkedEdgeSet;	
	}

	public List<N> getPath(N source, N target) {
		path.add(source);
		this.visitNodeField(source);
		this.iterateLinked(source, target);
		
		if(this.findNodeField(target).checkVisit() == false) {
			System.out.println("No available path between "+ source + " and " + target);
			path.removeAll(path);
		}
		
		return path;
		
	}
	
	
	private N evaluateNodes(Edge nodePair, N node) {
		if(nodePair.getNode1().getNodeType() == node) {
			return  (N) nodePair.getNode2().getNodeType();
		}

		return null;
	}
	
	private void iterateLinked(N source, N target) {
		
		for(N linked: linkedNodes(source)) {
			if(this.findNodeField(target).checkVisit()) {
				break;
			}
			else {
				if(linked == target) {
					path.add(target);
					visitNodeField(target);
				}
				else if(!this.findNodeField(linked).checkVisit()){
					path.add(linked);
					this.visitNodeField(linked);
					
					if(!this.leftToVisit(linked)) {
						path.remove(linked);
					}
					else {
						this.iterateLinked(linked, target);
					}
				}
				else {
					continue;
				}
			}
		}
	}
	
	private Node findNodeField(N linked) {
		for(Node node: nodeSet) {
			if(node.getNodeType() == linked) {
				return node;
			}
		}
		return null;
	}
	
	private void visitNodeField(N linked) {
		findNodeField(linked).visitNode();
	}
	
	private boolean leftToVisit(N node) {
		for(N linked: linkedNodes(node)) {
			if(!this.findNodeField(linked).checkVisit()) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean emptyWalk(N node) {
		return this.linkedNodes(node).isEmpty();
	}

	
}
